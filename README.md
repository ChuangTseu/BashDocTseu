# BashDocTseu

## Install

1. Download or clone anywhere
2. Put the folder into your PATH
3. [OPTIONAL] To get bash completion: source `complete/bashdoctseu` in your .bashrc

## Usage

Use `--help` to get help for each doc command.

Examples:
```bash
$ docgit.sh --help
$ docbash.sh --help
```
