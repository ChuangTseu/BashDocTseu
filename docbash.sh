#!/usr/bin/env bash

usage_full()
{
    COMMON_usage_basic

cat <<EOL
These are the commands or concepts you can get quick help about:

Concepts/commands:

   grep_echo_matching_only  How to grep with modern regexes and output only the
                            matches (cut++)
EOL
}

__docbash_fn__grep_echo_matching_only()
{
$DOC_PAGER <<EOL
${CATEGORY}TL;DR:${NC}
${PROMPT_LINE} grep -oP "\${your_perl_regex}" ${NC}

${CATEGORY}OPTIONS:${NC}
Use option -o to print matches only.
Use option -P to use the advanced Perl regex syntax.

${CATEGORY}TIPS:${NC}
Make use of lookahead/lookbehind to isolate the wanted match.

${CATEGORY}EXAMPLES:${NC}
${PROMPT_LINE} echo "[--bob] [--easy] <command>" | grep -oP "(?<=\[)-.*?(?=\])" ${NC}
--bob
--easy

${YELLOW}REFS:${NC}
${LINK}https://unix.stackexchange.com/a/13472${NC}
EOL
}


set -e
source "${BASH_SOURCE%/*}/lib/common.sh"

main()
{
    COMMON_main "$@"
}

main "$@"
