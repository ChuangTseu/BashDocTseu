#!/usr/bin/env bash

usage_full()
{
    COMMON_usage_basic

cat <<EOL
These are the commands or concepts you can get quick help about:

Concepts/commands:

   amend                    How to commit with the --amend option
   branch_create            How to create a branch
   branch_setup             How to setup a branch after creation (ex: change
                            remote tracking)
   cherry_pick              How to use the cherry-pick command
   lean_cloning             How to clone with only the required branches in the
                            leanest way possible
   parents                  A very quick reminder of how ~ and ^ work
   push_force_with_lease    How to force push the correct way using
                            --force-with-lease
   rebase_interactive       How to rebase in interactive mode (-i or
                            --interactive)
   reset                    How to use reset with its different modes (--soft,
                            --mixed, --hard)
EOL
}


__docgit_fn__amend()
{
$DOC_PAGER <<EOL
${CATEGORY}TL;DR:${NC}
${PROMPT_LINE} git commit --amend ${NC}
EOL
}

__docgit_fn__branch_create()
{
$DOC_PAGER <<EOL
${CATEGORY}TL;DR:${NC}
Creating a branch?
${PROMPT_LINE} git branch \${your_branch_name} ${NC}

Creating a branch AND instantly checking it out?
${PROMPT_LINE} git checkout -b \${your_branch_name} ${NC}
EOL
}

__docgit_fn__branch_setup()
{
$DOC_PAGER <<EOL
WORK IN PROGRESS...
EOL
}

__docgit_fn__cherry_pick()
{
$DOC_PAGER <<EOL
WORK IN PROGRESS...
EOL
}

__docgit_fn__lean_cloning()
{
$DOC_PAGER <<EOL
WORK IN PROGRESS...
EOL
}

__docgit_fn__parents()
{
$DOC_PAGER <<EOL
${CATEGORY}GIST:${NC}
${BOLD}~${NC} stands for ${BOLD}ANCESTOR${NC}.
    Think father, grandfather, and so on.

${BOLD}^${NC} stands for ${BOLD}PARENT${NC}.
    Think mother, then father, then father's grandfather, and so on.
EOL
}

__docgit_fn__push_force_with_lease()
{
$DOC_PAGER <<EOL
${CATEGORY}TL;DR:${NC}
${PROMPT_LINE} git push --force-with-lease ${NC}

${CATEGORY}GIST:${NC}
WHEN FORCE PUSHING, ALWAYS USE ${BOLD}git push --force-with-lease${NC}

It will ensure that no one other than you pushed anything to the branch in the
meantime.

You shall never ever use --force alone. NEVER!

${YELLOW}REFS:${NC}
${LINK}https://developer.atlassian.com/blog/2015/04/force-with-lease/${NC}
EOL
}

__docgit_fn__rebase_interactive()
{
$DOC_PAGER <<EOL
WORK IN PROGRESS...
EOL
}

__docgit_fn__reset()
{
$DOC_PAGER <<EOL
WORK IN PROGRESS...
EOL
}

set -e
source "${BASH_SOURCE%/*}/lib/common.sh"

main()
{
    COMMON_main "$@"
}

main "$@"
