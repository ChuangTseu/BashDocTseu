#!/usr/bin/env bash

# Colors and styling for use in output
RED=$(echo -e "\e[31m")
GREEN=$(echo -e "\e[32m")
YELLOW=$(echo -e "\e[33m")
BLUE=$(echo -e "\e[34m")
NC=$(echo -e "\e[0m")

BOLD=$(echo -e "\e[1m")
UNDERLINED=$(echo -e "\e[4m")

CATEGORY="${YELLOW}"
LINK="${UNDERLINED}"
PROMPT="${GREEN}\$${NC}"
PROMPT_LINE="${GREEN}\$${NC}${BOLD}"


# Default pager is less, no paging is done if the output fits in on screen
DOC_PAGER="less -F -X -r"


COMMON_usage_basic()
{
cat <<EOL
Usage: $(basename "$0") [--help] [--no-pager] <command_or_concept>

EOL
}


COMMON_main()
{
    # Consume options
    while [[ "$#" > 0 && "$1" =~ ^-.* ]] ; do
        case "$1" in
            "--help")
                usage_full
                return 0
                ;;
            "--no-pager")
                DOC_PAGER="cat"
                ;;
            *)
                echo "Unknown option: $1"
                usage_basic
                return 1
                ;;
        esac

        shift
    done


    if [[ "$#" == 0 || "$1" == "help" ]]; then
        usage_full
        return 0
    fi

    if [[ "$#" > 1 ]]; then
        echo "Error: Only a single concept/command with no options is allowed, you provided $# arguments"
        usage_basic
        return 1
    fi


    local cmd="$1"

    local script_basename="$(basename "$0")"
    local script_name_noext="${script_basename%.*}"

    local mangled_docfn="__${script_name_noext}_fn__$cmd"

    # If you want your function to be callable, simply prepend __${YOUR_SCRIPT_NAME}_fn__ to your function name
    # Make sure to document it properly in the usage_full function
    if [[ "$(type -t "$mangled_docfn")" == "function" ]] ; then
        "$mangled_docfn"
    else
        echo "Unknown command or concept: '$cmd'. See '$0 --help'."
    fi
}
